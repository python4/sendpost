#!/usr/bin/env python3

import requests
import urllib
import urllib3
import json
requests.packages.urllib3.disable_warnings()
import argparse

parser = argparse.ArgumentParser(description='Send post to mattermost')
# Soit fichier yaml
parser.add_argument('--url',help="Url d'envoi",default="https://login:pass@url.mattermost.org/team/channel?user_to_send_to",required=True)
parser.add_argument('--message',help="Message à envoyer",default="Tout est Ok ",required=True)

args = parser.parse_args()

url = args.url
message = args.message

# GET Variables
apiurl = urllib3.util.parse_url(url)
baseurl = "%s://%s" % (apiurl.scheme,apiurl.host)
login = urllib.parse.unquote(apiurl.auth.split(":")[0])
passwd = urllib.parse.unquote(apiurl.auth.split(":")[1])
team = apiurl.path.split('/')[1]
channel = apiurl.path.split('/')[2]
user = apiurl.query
#   print("""
#           team: %s,
#           channel: %s,
#           user: %s
#           """ % (team,channel,user)
#   )

# Set first Headers
headers = {"Content-Type": "application/json"}

# Get logged
url = "%s/api/v4/users/login" % baseurl
payload = {
        "login_id": login,
        "password": passwd
}
response = requests.request("POST", url, json=payload, headers=headers)
token = response.headers['Token']

# Set logged headers
headers = {
    "Content-Type": "application/json",
    "Authorization": "Bearer %s" % token
}

# Get team ID
url = "%s/api/v4/teams/name/%s" % (baseurl, team)
response = requests.request("GET", url, headers=headers)
teamid = response.json()["id"]

# Get channem ID
url = "%s/api/v4/teams/%s/channels/name/%s" % (baseurl,teamid,channel)
response = requests.request("GET", url, headers=headers)
channelid=response.json()['id']

# Send post to channel and user
url = "%s/api/v4/posts" % baseurl
payload = {
    "channel_id": "%s" % channelid,
    "message": "@%s : %s" % (user,message)
}
response = requests.request("POST", url, json=payload, headers=headers)
print(response)
print(response.text)
print(json.dumps(response.json(),indent=2))


